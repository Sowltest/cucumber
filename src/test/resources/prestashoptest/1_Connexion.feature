# language: en
Feature: Connexion

	Scenario Outline: Connexion
		Given je me trouve sur la page d'authentification du site
		When je saisis mon <id> et mon <mdp>
		And Je clique sur le bouton Connexion du formulaire
		Then un <resultat>

		@cas_passant
		Examples:
		| id | mdp | resultat |
		| "j2ee" | "j2ee" | "La page d'accueil s'affiche" |