package CucumberJava.stepsDefinition;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import pageObjects.accueilPage;
import pageObjects.connexionPage;
import io.github.bonigarcia.wdm.WebDriverManager;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class Stepdefinition {
    WebDriver driver;

    @Before
    void setup() {
        driver = WebDriverManager.chromedriver().create();
    }

    @After
    void teardown() {
        driver.quit();
    }

    @Given("je me trouve sur la page d'authentification du site")
    public void je_me_trouve_sur_la_page_d_authentification_du_site() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/drivers/chromedriver.exe");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");
        driver = new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(2));
        driver.get("https://petstore.octoperf.com/actions/Catalog.action");
        accueilPage Accueil = new accueilPage(driver);
        Accueil.signInButton.click();
    }
    @When("je saisis mon {string} et mon {string}")
    public void je_saisis_mon_et_mon(String string, String string2) {
        System.out.println("Send key");
        connexionPage Connexion = new connexionPage(driver);
        Connexion.usernameChamp.click();
        Connexion.usernameChamp.clear();
        Connexion.usernameChamp.sendKeys(string);
        Connexion.passwordChamp.click();
        Connexion.passwordChamp.clear();
        Connexion.passwordChamp.sendKeys(string2);
    }
    @When("Je clique sur le bouton Connexion du formulaire")
    public void je_clique_sur_le_bouton_connexion_du_formulaire() {
        System.out.println("Connexion");
        connexionPage Connexion = new connexionPage(driver);
        Connexion.loginButton.click();
    }
    @Then("un {string}")
    public void un(String string) {
        System.out.println("Assert");
        accueilPage Accueil = new accueilPage(driver);
        assertTrue(Accueil.signOutButton.isDisplayed());
    }
    


}
