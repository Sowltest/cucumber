package pageObjects;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class connexionPage extends abstractPage {
    // ********* Constructor ******** //
    public connexionPage(WebDriver driver){
        super(driver);
        PageFactory.initElements(driver, this);
    }

    //WebElements

    //champ username
    @FindBy(xpath="//input[@name=\"username\"]")
    public WebElement usernameChamp;

    //Champ password
    @FindBy(xpath="//input[@name=\"password\"]")
    public WebElement passwordChamp;

    //Bouton Login
    @FindBy(xpath="//input[@name=\"signon\"]")
    public WebElement loginButton;
}
