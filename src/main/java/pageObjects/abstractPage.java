package pageObjects;

import org.openqa.selenium.WebDriver;

    public class abstractPage{

        // ***** Variables ***** //
        protected final WebDriver driver;

        // ***** Constructeur ***** //
        public abstractPage (WebDriver driver){
            this.driver = driver;
        }

    }
