package pageObjects;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class accueilPage extends abstractPage {
    // ********* Constructor ******** //
    public accueilPage(WebDriver driver){
        super(driver);
        PageFactory.initElements(driver, this);
    }

    //WebElements

    //Bouton Sign In
    @FindBy(xpath="//a[contains(text(),\"Sign In\")]")
    public WebElement signInButton;

    //Bouton Sign In
    @FindBy(xpath="//a[contains(text(),\"Sign Out\")]")
    public WebElement signOutButton;


}
